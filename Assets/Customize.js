$(document).ready(function() {
  $(".btnToggleNavs").on("click", function() {
    $("#navbar").slideToggle("slow");
  });

  /* Page: Index */

  $(".btnToggleMostProductChart").on("click", function() {
    $(".MostProductChart").fadeToggle();
    // Veriler otomatik olarak çekilip, chart'a entegre edilecek.
  });

  // Chart test
  var chartElement = $("#myChart");
  var myChart = new Chart(chartElement, {
    type: "pie",
    data: {
      labels: ["Elma", "Armut", "Karpuz", "Portakal"],
      datasets: [
        {
          label: "Satış Adeti",
          data: [12, 4, 65, 48].sort((a, b) => {
            return b - a;
          }),
          backgroundColor: [
            "#454545",
            "rgba(54, 162, 235, 0.2)",
            "rgba(255, 206, 86, 0.2)",
            "#547fac"
          ],
          borderColor: [
            "rgba(255, 99, 132, 1)",
            "rgba(54, 162, 235, 1)",
            "rgba(255, 206, 86, 1)",
            "rgba(255, 206, 86, 1)"
          ],
          borderWidth: 1
        }
      ]
    }
  });
  /* Page: Index */

  /* Page: Products */
  // $("#product-list-table").DataTable();
  $(".product_sorting_option").on("change", function() {
    var option = $(this).val();
    if (option.length > 0) {
      switch (option) {
        case "product-category":
          $(".pCategorySorting").show("slow");
          $(".sCountSorting").hide("slow");
          break;
        case "sell-count":
          $(".pCategorySorting").hide("slow");
          $(".sCountSorting").show("slow");
          break;
      }
    }
  });
  /*
  DÜZENLENECEK
  var i = 1;
  $(".btnAddCategory").on("click", function() {
    i++;
    var categoryField = '<div class="col-9 my-2" id="row' + i + '">';
    categoryField +=
      ' <select name="" id="" class="form-control">  <option value="">Kategori Adı</option> </select>';
    categoryField += "</div>";
    categoryField +=
      ' <div class="col-3 my-2" id="row' +
      i +
      '"><button data-row-id="' +
      i +
      '" class="btn btnRemoveCategory btn-dark btn-block"><i class="fas fa-minus"></i></button> </div> ';
    $(".product-categories").append(categoryField);
  });
  $(document).delegate(".btnRemoveCategory", "click", function() {
    var row_id = $(this).data("row-id");
    $("#row" + row_id + " ").remove();
  });
  */

  $(".uploadNewImageButton").on("click", function() {
    $(".uploadNewImage").click();
  });

  $(".transaction_select").on("change", function() {
    var transaction = $(this).val();
    switch (transaction) {
      case "create-product":
        $(".transactionBox").hide("slow");
        $(".tr-create-product").show("slow");
        break;
      case "product-property":
        $(".transactionBox").hide("slow");
        $(".tr-product-properties").show("slow");
        break;
      case "product-details":
        $(".transactionBox").hide("slow");
        $(".tr-product-detail").show("slow");
        break;
      default:
        $(".transactionBox").hide("slow");
        break;
    }
  });

  $(document).delegate(".product-search-input", "change keyup", function() {
    $(".search-result").show("slow");
  });

  $(document).delegate(".showDetailBtn", "click", function() {
    $(".search-result").hide("slow");
    $(".product-detail").show("slow");
  });

  $(".showModal").modal({ show: true });

  /* Page: Products */
});
